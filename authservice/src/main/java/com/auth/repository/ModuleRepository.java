package com.auth.repository;

import com.auth.entity.Module;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ModuleRepository extends JpaRepository<Module,Integer>{
    Optional<Module> findByModuleSeq(Integer moduleSeq);

    List<Module> findByStatus(Integer status);

    Module findByModuleCodeAndStatus(String moduleCode, Integer status);
}
