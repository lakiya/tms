package com.auth.service;

import com.auth.entity.Module;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public interface ModuleService {
    ResponseEntity<Module> createModule(Module module);

    ResponseEntity<Module> updateModule(Module module);

    ResponseEntity<Module> deleteModule(Integer moduleSeq);
}
