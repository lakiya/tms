package com.auth.serviceImpl;

import com.auth.entity.Module;
import com.auth.repository.ModuleRepository;
import com.auth.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ModuleServiceImpl implements ModuleService {

    private final ModuleRepository moduleRepository;

    @Autowired
    public ModuleServiceImpl(ModuleRepository moduleRepository) {
        this.moduleRepository = moduleRepository;
    }

    @Override
    public ResponseEntity<Module> createModule(Module module) {
        module = this.moduleRepository.save(module);
        return new ResponseEntity<>(module, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Module> updateModule(Module module) {
        Optional<Module> dbModule = this.moduleRepository.findByModuleSeq(module.getModuleSeq());
        ResponseEntity<Module> responseEntity;
        if (dbModule.isPresent()) {
            if (dbModule.get().equals(module)) {
                responseEntity = new ResponseEntity<>(module, HttpStatus.NOT_MODIFIED);
            } else {
                module = this.moduleRepository.save(module);
                responseEntity = new ResponseEntity<>(module, HttpStatus.OK);
            }
        } else {
            responseEntity = new ResponseEntity<>(module, HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    public ResponseEntity<Module> deleteModule(Integer moduleSeq) {
        Optional<Module> dbModule = this.moduleRepository.findByModuleSeq(moduleSeq);
        ResponseEntity<Module> responseEntity;
        if (dbModule.isPresent()) {
            responseEntity = new ResponseEntity<>(HttpStatus.OK);
        } else {
            responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }
}
