package com.auth.controller;

import com.auth.entity.Module;
import com.auth.repository.ModuleRepository;
import com.auth.service.ModuleService;
import com.auth.util.MasterDataStatus;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("modules")
public class ModuleController {

    private final ModuleService moduleService;
    private final ModuleRepository moduleRepository;

    @Autowired
    public ModuleController(ModuleService moduleService,
                            ModuleRepository moduleRepository) {
        this.moduleService = moduleService;
        this.moduleRepository = moduleRepository;
    }

    @PostMapping
    public ResponseEntity<Module> createModule(Module module){
        return this.moduleService.createModule(module);
    }

    @PutMapping
    public ResponseEntity<Module> updateModule(Module module){
        return this.moduleService.updateModule(module);
    }

    @DeleteMapping("{moduleSeq}")
    public ResponseEntity<Module> deleteModule(Integer moduleSeq){
        return this.moduleService.deleteModule(moduleSeq);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Module> findModules(){
        return this.moduleRepository.findByStatus(MasterDataStatus.APPROVED.getStatusSeq());
    }

    @GetMapping("{moduleSeq}")
    public Module findModuleByModuleSeq(@PathVariable("moduleSeq") Integer moduleSeq){
        return this.moduleRepository.findOne(moduleSeq);
    }

    @GetMapping(params = "moduleCode")
    public Module findModuleByModuleCode(@RequestParam("moduleCode") String moduleCode){
        return this.moduleRepository.findByModuleCodeAndStatus(moduleCode, MasterDataStatus.APPROVED.getStatusSeq());
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
