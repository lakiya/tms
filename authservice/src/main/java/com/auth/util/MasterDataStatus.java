package com.auth.util;

public enum MasterDataStatus {
    DELETED(0, "Deleted"),
    OPEN(1, "Open"),
    APPROVED(2, "Approved"),
    CLOSED(3, "Closed");

    private final Integer statusSeq;
    private final String status;

    MasterDataStatus(Integer statusSeq, String status) {
        this.statusSeq = statusSeq;
        this.status = status;
    }

    public Integer getStatusSeq() {
        return statusSeq;
    }

    public String getStatus() {
        return status;
    }
}
